SELECT 
	user.`id`,
	user.`username`,
	user.`avatar`,
	user.`fname`,
	user.`lname`,
	user.`street`,
	user.`city`,
	user.`state`,
	user.`zip_postal`,
	user.`phone`,
	user.`username`

	FROM `{%t_users%}` as user
	
	WHERE user.`active` = '1'

	AND user.`verified` = '1'

	AND user.`is_seller` = 'Y'

	AND user.`lat` BETWEEN {%minlat%} AND {%maxlat%}
	AND user.`lon` BETWEEN {%minlng%} AND {%maxlng%}

{%if limit%}	
	LIMIT {%limit%}
{%endif%}