<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)									@
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@


# MySQL db host name
$sql_db_host = "localhost";
# MySQL db User
$sql_db_user = "root";
# MySQL db Password
$sql_db_pass = "";
# MySQL db Name
$sql_db_name = "onelife";
# Site URL
$site_url    = "http://localhost/onelifehair/onelifehair"; // E.g (http://example.com)
?>