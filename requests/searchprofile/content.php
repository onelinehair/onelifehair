<?php
# @*************************************************************************@
# @ @author Mansur Altamirov (Mansur_TL)                                    @
# @ @author_url 1: https://www.instagram.com/mansur_tl                      @
# @ @author_url 2: http://codecanyon.net/user/mansur_tl                     @
# @ @author_email: highexpresstore@gmail.com                                @
# @*************************************************************************@
# @ HighExpress - The Ultimate Modern Marketplace Platform                  @
# @ Copyright (c) 05.07.19 HighExpress. All rights reserved.                @
# @*************************************************************************@

if (empty($hs['is_logged'])) {
	hs_redirect('auth');
}

$hs['page_title']     = hs_translate('Search profiles - {%name%}',array('name' => $hs['config']['name']));
$hs['page_desc']      = $hs['config']['description'];
$hs['page_kw']        = $hs['config']['keywords'];

$lat              	  = (not_empty($_POST['lat'])) ? hs_secure($_POST['lat']) : null;
$lng              	  = (not_empty($_POST['lng'])) ? hs_secure($_POST['lng']) : null;
$hs['header_st']      = false;
$hs['pn']             = 'wishlist';
$hs['list_data']      = array();
$my_id                = $me['id'];


$hs['list_users'] =  hs_search_profiles(array(
	'user_id'     => $my_id,
	'lat'         => $lat,
	'lng'         => $lng
));


$hs['site_content'] =  hs_loadpage('searchprofiles/content',array(
	'list_users'    => $hs['list_users']
));